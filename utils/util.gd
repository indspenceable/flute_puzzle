class_name Util

static func approach(original_value :float, target_value :float, max_delta: float) -> float:
  if abs(original_value - target_value) < max_delta:
    return target_value
  elif target_value > original_value:
    return original_value + max_delta
  else:
    return original_value - max_delta

static func unique(ary: Array) -> Array:
  var d : Dictionary = {}
  var rtn = []
  for el in ary:
    if !d.has(el):
      rtn.append(el)
    d[ary] = true
  return rtn

static func compare(ary1:Array, ary2:Array) -> Dictionary:
  var f = []
  var l = []
  var c = []
  var all_elements := []
  all_elements.append_array(ary1)
  all_elements.append_array(ary2)
  for el in unique(all_elements):
    var a = el in ary1
    var b = el in ary2
    var z = el in all_elements
    if a && b:
      c.append(el)
    elif a:
      f.append(el)
    elif b:
      l.append(el)
    else:
      printerr("An element being compared is is not in either input.")
  return {
    "former": f,
    "latter": l,
    "common": c
   }
