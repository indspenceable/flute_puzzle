extends Node

var current_scene_path : String
var player_destination_position : Vector2
var loaded = false

func swap_to_scene(path: String, position :Vector2) -> void:
  loaded = true
  current_scene_path = path
  player_destination_position = position
  get_tree().change_scene_to_file(path)

func reload_scene()->void:
  swap_to_scene(current_scene_path, player_destination_position)
