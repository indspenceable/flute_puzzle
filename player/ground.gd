extends PlayerState
class_name GroundState

var jump_buffer : bool
var motion : Vector2

func enter(msg := {}) -> void:
  msg.merge({
    "motion": Vector2.ZERO,
    "jump_buffer": false,
  })
  motion = msg.motion
  jump_buffer = msg.jump_buffer

  var moving : bool = abs(motion.x) > 1
#  player.animation_tree.set("parameters/conditions/land_move", moving)
#  player.animation_tree.set("parameters/conditions/land_idle", !moving)
#  player.animation_tree.set("parameters/conditions/rising", false)
#  player.animation_tree.set("parameters/conditions/falling", false)
  if not player.grounded():
    fall()
    return

func exit() -> void:
  pass
#  $MovementDust.emitting = false
#  $Walk.stop()
#  player.animation_tree.set("parameters/conditions/land_move", false)
#  player.animation_tree.set("parameters/conditions/land_idle", false)

func tick(_delta: float) -> void:
  var moving : bool = abs(motion.x) > 1
  var anim = "walk" if moving else "idle"
  player.animation_player.current_animation = anim
#  player.animation_tree.set("parameters/conditions/land_move", moving)
#  player.animation_tree.set("parameters/conditions/land_idle", !moving)

func fall(additional_motion : Vector2 = Vector2.ZERO) -> void:
  state_machine.transition_to("Air", {
    "motion": motion + additional_motion,
    "coyote_time": player.coyote_time
  })

static func grounded_movement(delta: float, player: Player, _motion: Vector2, apply_friction: bool ) -> Vector2:
  if apply_friction:
    _motion.x = Util.approach(_motion.x, 0, player.run_reduce_speed * delta)
  player.move_by_vector(_motion * delta)
  player.snap_to_ground()
  return _motion

func physics_tick(delta: float) -> void:
  var af = false
  if Input.is_action_pressed("right"):
    var turnaround_multiplier = 1
    if motion.x < 0:
      turnaround_multiplier = 2.5

    if motion.x <  player.movespeed:
      motion.x = Util.approach(motion.x,  player.movespeed, player.acceleration * delta * turnaround_multiplier)
    else:
      motion.x = Util.approach(motion.x,  player.movespeed, player.run_reduce_speed * delta)
    player.facing_right = true
  elif Input.is_action_pressed("left"):
    var turnaround_multiplier = 1
    if motion.x > 0:
      turnaround_multiplier = 2.5

    if motion.x > - player.movespeed:
      motion.x = Util.approach(motion.x, - player.movespeed, player.acceleration * delta * turnaround_multiplier)
    else:
      motion.x = Util.approach(motion.x, - player.movespeed, player.run_reduce_speed * delta)
    player.facing_right = false
  else:
    motion.x = Util.approach(motion.x, 0, player.run_reduce_speed * delta)

    # are we trying to jump
  if Input.is_action_just_pressed("jump") || jump_buffer:
    if Input.is_action_pressed("down") && player.standing_on_oneway():
      player.position.y += player.pass_thru_help_distance
      motion.y = player.pass_thru_help_distance
      state_machine.transition_to("Air", { "motion": motion})
    else:
      # drop the y of our motion - our grounded y shouldn't be
      # positive.
      if player.current_attachment is MotionBody:
        motion += player.current_attachment.motion

      state_machine.transition_to("Air", {
        "motion": Vector2(motion.x, 0),
        "jump_strength": Vector2(0, -player.jump_strength)
      })
    return

  jump_buffer = false

  motion.y = 0
  
  motion = grounded_movement(delta, player, motion, af)
  if player.check_right() && motion.x > 0:
    motion.x = 0
  
  if player.check_left() && motion.x < 0:
    motion.x = 0
    
  if Input.is_action_just_pressed('down'):
    state_machine.transition_to("Flute")
    return
  
  if not player.grounded():
    fall(Vector2(0, 0))
    return
