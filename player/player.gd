extends PlatformBody
class_name Player

@export var coyote_time := 0.2
@export var run_reduce_speed := 500
@export var jump_peak_gravity_threshhold := 20
@export var jump_peak_gravity := 0.4
@export var rising_gravity := 600
@export var falling_gravity := 900
@export var max_gravity := 600
@export var movespeed := 600
@export var acceleration := 900
@export var jump_strength := 900
@export var jump_stop_reduction_percent := 0.5
@export var jump_buffer = 0.2
@export var pass_thru_help_distance = 4
var facing_right = false
var current_attachment

@onready var animation_player : AnimationPlayer = $AnimationPlayer

func _ready() -> void:
  $AnimationPlayer.current_animation = "idle"

func _physics_process(delta: float) -> void:
  var world = get_world_2d().direct_space_state
  var params := PhysicsPointQueryParameters2D.new()
  params.collision_mask = 0b100
  
  params.position = global_position + Vector2(0,0)
  if world.intersect_point(params).size() > 0:
    Globals.reload_scene()
  
  params.position = global_position + Vector2(0,-6)
  if world.intersect_point(params).size() > 0:
    Globals.reload_scene()
