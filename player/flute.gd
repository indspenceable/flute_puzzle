extends PlayerState

@export var note_textures : Array[Texture2D]
var current_song : Array[int]
@onready var rects : Array[TextureRect] = [] + $Reset/PanelContainer/HBoxContainer.get_children()

func setup() -> void:
  exit()

func enter(message={})->void:
  $Reset/PanelContainer.visible = true
  current_song = []
  Engine.time_scale = 0
  display_current_song()

func exit() -> void:
  $Reset/PanelContainer.visible = false
  Engine.time_scale = 1

func physics_tick(delta: float) -> void:
  if Input.is_action_just_pressed("up"):
    play_note(0)
  if Input.is_action_just_pressed("left"):
    play_note(1)
  if Input.is_action_just_pressed("right"):
    play_note(2)
  if Input.is_action_just_pressed("down"):
    play_note(3)
    
  if Input.is_action_just_pressed("esc"):
    state_machine.transition_to("Ground")

func play_note(i):
  current_song.append(i)
  display_current_song()
  
  var any_success = false
  
  for n in get_tree().get_nodes_in_group('global_song_listener'):
    any_success = n.hear_song_global(current_song) || any_success
  for n in get_tree().get_nodes_in_group('local_song_listener'):
    print(player.global_position)
    if n.contains_pt(player.global_position):
      any_success = n.hear_song_local(current_song) || any_success
  
  if any_success:
    state_machine.transition_to("Wait", {'duration': 0.25})
    
func display_current_song():
  var i = 0
  while i < rects.size():
    if current_song.size() > i:
      rects[i].texture = note_textures[current_song[i]]
    else:
      rects[i].texture = null
    i += 1
