# Virtual base class for all states.
class_name State
extends Node2D

# RefCounted to the state machine, to call its `transition_to()` method directly.
# That's one unorthodox detail of our state implementation, as it adds a dependency between the
# state and the state machine objects, but we found it to be most efficient for our needs.
# The state machine node will set it.
var state_machine : StateMachine = null

# Virtual function. Receives events from the `_unhandled_input()` callback.
func handle_input(_event: InputEvent) -> void:
  pass

# Virtual function. Corresponds to the `_process()` callback.
func tick(_delta: float) -> void:
  pass

# Virtual function. Corresponds to the `_physics_process()` callback.
func physics_tick(_delta: float) -> void:
  pass

