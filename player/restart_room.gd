extends Node

@export var song_pattern : Array[int]

func hear_song_global(song) -> bool: 
  if song == song_pattern:
    Globals.reload_scene()
    return true
  return false
