extends PlayerState


func enter(msg: Dictionary) -> void:
  msg.merge({
    'duration': 0
  })
  get_tree().create_timer(msg.duration).connect("timeout", transition_out)

func exit():
  pass

var air_motion := Vector2.ZERO

func physics_tick(delta: float):
  if player.grounded():
    GroundState.grounded_movement(delta, player, Vector2.ZERO, true)
  else:
    air_motion = AirState.air_movement(delta, player, player.falling_gravity, air_motion, true)

func transition_out():
  state_machine.transition_to("Ground" if player.grounded() else "Air")
