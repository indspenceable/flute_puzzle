extends PlayerState
class_name AirState

var motion : Vector2
var coyote_time : float
var jump_buffer : float
var jump_is_held : bool
var air_jumps : int


var water_mult : float = 0.6
var currently_in_water : bool
# expecting:
# motion, coyote_time, air_jumps,
# and possibly jump_strength, set_facing

func enter(msg := {}) -> void:
  msg.merge({
    "motion": Vector2.ZERO,
    "coyote_time": 0.0,
    "air_jumps": 0,
  })
  motion = msg.motion
  coyote_time = msg.coyote_time
  jump_buffer = 0
  jump_is_held = false
  air_jumps = msg.air_jumps
  if msg.has("jump_strength"):
    # we're jumping! we start from a zero y always
    # but we don't need to wipe out the x
    motion.y = 0
    motion += msg.jump_strength
    jump_is_held = true
#    $AirJumpDust.restart()
#    $Jump.play()

  currently_in_water = in_water()

  update_animator()

func update_animator():
  pass
#  player.animation_tree.set("parameters/conditions/rising", motion.y < 0)
#  player.animation_tree.set("parameters/conditions/falling", motion.y >= 0)

func exit() -> void:
  pass
#  player.animation_tree.set("parameters/conditions/rising", false)
#  player.animation_tree.set("parameters/conditions/falling", false)

func ipp(pt: Vector2, mask: int) -> PhysicsPointQueryParameters2D:
  var params := PhysicsPointQueryParameters2D.new()
  params.position = pt
  params.collision_mask = mask
  params.collide_with_areas = true
  params.collide_with_bodies = true
  return params

func in_water() -> bool:
  var space_state : PhysicsDirectSpaceState2D = get_world_2d().direct_space_state
  return len(space_state.intersect_point(ipp(player.global_position, 0b100000000), 1)) > 0

func gravity() -> float:
  var multiplier : float = 1.0 if abs(motion.y) > player.jump_peak_gravity_threshhold else player.jump_peak_gravity
  multiplier *= water_mult if in_water() else 1.0
  if motion.y < 0:
    return player.rising_gravity * multiplier
  else:
    return player.falling_gravity * multiplier

static func air_movement(delta: float, player: Player, gravity: float, _motion: Vector2, apply_friction: bool) -> Vector2:
  var acceleration := gravity*delta
  _motion.y = [_motion.y + acceleration, player.max_gravity].min()

  if apply_friction && false:
    var s = 1 if _motion.x < 0 else -1
    var f = player.friction_air * s * delta
    if abs(_motion.x) < abs(f):
      _motion.x = 0
    else:
      _motion.x += f

  var mv = player.move_by_vector(_motion * delta + Vector2.DOWN*acceleration*delta)
  if mv.l && _motion.x < 0:
    _motion.x = 0
  if mv.r && _motion.x > 0:
    _motion.x = 0
  if mv.u && _motion.y < 0:
    _motion.y = 0
  return _motion

func physics_tick(delta: float) -> void:
  var apply_friction = false
  if Input.is_action_pressed("right"):
    if motion.x <  player.movespeed:
      motion.x = Util.approach(motion.x,  player.movespeed, player.acceleration * 0.65 * delta)
    else:
      motion.x = Util.approach(motion.x,  player.movespeed, player.run_reduce_speed * 0.65 * delta)
    player.facing_right = true
  elif Input.is_action_pressed("left"):
    if motion.x > - player.movespeed:
      motion.x = Util.approach(motion.x, - player.movespeed, player.acceleration * 0.65 * delta)
    else:
      motion.x = Util.approach(motion.x, - player.movespeed, player.run_reduce_speed * 0.65 * delta)
    player.facing_right = false
  else:
    motion.x = Util.approach(motion.x, 0, player.run_reduce_speed * 0.65 * delta)
    #apply_friction = true #motion.x = lerp(motion.x, 0, clamp(delta * player.friction_air, 0, 1))

  if Input.is_action_just_released("jump"):
    jump_buffer = 0.0
    if jump_is_held:
      motion.y = motion.y * player.jump_stop_reduction_percent
    jump_is_held = false

  jump_buffer -= delta
  coyote_time -= delta
  
  motion = air_movement(delta, player, gravity(), motion, apply_friction)
  if Input.is_action_just_pressed("jump"):
    # if we're in coyote time, do a regular jump.
    if coyote_time > 0:
      state_machine.transition_to("Air", { "motion": motion, "jump_strength": Vector2(0, -player.jump_strength), "air_jumps": air_jumps })
      return
    elif air_jumps > 0:
      # do an airjump as long as theres one remaining
      state_machine.transition_to("Air", { "motion": motion, "jump_strength": Vector2(0, -player.air_jump_strength), "air_jumps": air_jumps-1 })
      return
    else:
      jump_buffer = player.jump_buffer

  var can_slide : bool = false #SaveState.get_value_or('slide', false)
  var can_climb : bool = false #SaveState.get_value_or('climb', false)
  if player.grounded() && motion.y >= 0:
    # disabled for now
    if false && motion.y > player.max_gravity * 0.98:
      get_tree().root.get_node('Gameplay').camera.shake.shake(5, 0.2)
      state_machine.transition_to("InputDisabled", { "motion": motion, "jump_buffer": jump_buffer > 0, "delay": 0.5})
    else:
      state_machine.transition_to("Ground", { "motion": motion, "jump_buffer": jump_buffer > 0 })
    return

#  if player.ladder.ladder_detected():
#    if Input.is_action_pressed("up") or Input.is_action_pressed("down"):
#      state_machine.transition_to("LadderClimb")
#      return

  if in_water() != currently_in_water:
    $AirJumpDust.restart()
    currently_in_water = !currently_in_water
    $Splash.play()

func tick(_delta):
  update_animator()
