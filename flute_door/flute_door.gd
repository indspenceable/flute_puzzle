extends LocalSongReciever
class_name FluteDoor

@export var dest_scene_path : String
@export var dest_position : Vector2

func GoToDest():
  Globals.swap_to_scene(dest_scene_path, dest_position)
