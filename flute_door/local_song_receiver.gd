extends Node2D
class_name LocalSongReciever

@export var song_pattern: Array[int]
@onready var r: Rect2 = Rect2(global_position - Vector2(8,8), Vector2(16,16))

signal song_detected

func contains_pt(pt)->bool:
  var rtn = r.has_point(pt)
  return r.has_point(pt)

func hear_song_local(song) -> void:
  if song == song_pattern:
    emit_signal("song_detected")
