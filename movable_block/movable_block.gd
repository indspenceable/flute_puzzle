extends Node2D

@export_node_path(Node2D) var initial_lock_point: NodePath
@onready var lock_point : LockPoint = get_node(initial_lock_point)
@export var slide_duration : float = 2
var move_up_song : Array[int] = [0,0,3,3,1,2,3,2]
var move_down_song : Array[int] = [0,0,3,3,0,3,2,2]
var moving = false

func _ready() -> void:
  global_position = lock_point.global_position

func updateLockPoint(lp)->void:
  lock_point = lp
  moving = false

func hear_song_global(song):
  if moving:
    return false
    
  var target_lock_point = null 
  var rtn = false
  if song == move_up_song:
    rtn = true
    if lock_point.up != null:
      target_lock_point = lock_point.get_node(lock_point.up)
  if song == move_down_song:
    rtn = true
    if lock_point.down != null:
     target_lock_point = lock_point.get_node(lock_point.down)
    
  if target_lock_point != null:
    moving = true
    var tween = get_tree().create_tween()
    tween.tween_property(self, "global_position", target_lock_point.global_position, slide_duration)
    tween.tween_callback(func(): updateLockPoint(target_lock_point))
    return true
  return rtn
