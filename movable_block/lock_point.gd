extends Node2D
class_name LockPoint

@export_node_path(Node2D) var up;
@export_node_path(Node2D) var down;
@export_node_path(Node2D) var left;
@export_node_path(Node2D) var right;
