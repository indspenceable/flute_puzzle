extends Node2D

@export_node_path(TileMap) var tiles_path : NodePath
@onready var tiles :TileMap = get_node(tiles_path) as TileMap
@export var song_pattern : Array[int]

@export var ice_altas_coords := Vector2i(0, 1)
@export var ice_source_id := 2
@export var water_altas_coords := Vector2i(4, 0)
@export var water_source_id := 2

func hear_song_global(song) -> bool: 
  if song == song_pattern:
    var used_cells = tiles.get_used_cells(0)
    var set = {}
    for cell in used_cells:
      if tiles.get_cell_atlas_coords(0, cell) == ice_altas_coords && \
        tiles.get_cell_source_id(0, cell) == ice_source_id:
        tiles.set_cell(0, cell, water_source_id, water_altas_coords)
    return true
  return false
