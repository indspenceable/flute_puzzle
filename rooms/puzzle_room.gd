extends Node2D

func _ready() -> void:
  if Globals.loaded:
    $Player.global_position = Globals.player_destination_position
