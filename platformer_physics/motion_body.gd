extends PlatformBody
class_name MotionBody

@export var falling_gravity: float = 1000.0
var motion : Vector2
var max_fall_rate = 345.0

func get_mask()->int:
  return 0b100000001

func get_all_ignorables():
  return [self]

func _process(delta):
  motion.y = Util.approach(motion.y, max_fall_rate, falling_gravity * delta)
  var rtn = move_by_vector(motion * delta)
  if rtn.d:
    motion.y = 0
  if rtn.l && motion.x < 0:
    motion.x = 0
  if rtn.r && motion.x > 0:
    motion.x = 0
