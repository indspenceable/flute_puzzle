extends Node2D
class_name BaseBody


const STEP = 0.1

signal changed_position(x, y)
signal changed_position_delta(x, y)

@export var up_shape: Shape2D
@export var down_shape: Shape2D
@export var left_shape: Shape2D
@export var right_shape: Shape2D

func get_all_ignorables() -> Array:
  return []

func check_collider(shape : Shape2D, mask :int = 1, offset : Vector2= Vector2.ZERO):
  var state := get_world_2d().direct_space_state

  var query := PhysicsShapeQueryParameters2D.new()
  query.set_shape(shape)
  query.collision_mask = mask
  query.exclude = get_all_ignorables()
  query.transform = Transform2D(0, global_position + offset)
  query.collide_with_areas = true
  var results = state.intersect_shape(query, 32)
  if len(results) > 0:
    return results[0].collider
  return null

func get_mask() -> int:
  return 1

func move_by_vector(vec:Vector2):
  var original_position = position;
  var state = get_world_2d().direct_space_state
  var rtn = {'l': false, 'r': false, 'u': false, 'd': false}
  var oneway_mask = 0b11

  var steps :float = 30

  var dx = 0
  var dy = 0

  for i in range(steps):
    position += Vector2(vec.x/steps, 0)
    dx += vec.x/steps

    # TODO - this used to check if you were grounded and skip this block
    # do we need that?
    position += Vector2(0, vec.y/steps)
    dy += vec.y/steps

    # eject from left wall
    while check_left():
      position.x += STEP
      dx += STEP
      rtn.l =true
    # eject from right wall
    while check_right():
      position.x -= STEP
      dx -= STEP
      rtn.r = true
    # eject from ceiling

    #Vector2(0,-16)
    while check_up():
      position.y += STEP
      dy += STEP
      rtn.u = true
    #eject from ground
    while check_down():
      position.y -= STEP
      dy -= STEP
      rtn.d = true

  emit_signal("changed_position", position.x, position.y)
  emit_signal("changed_position_delta", dx, dy)
  return rtn

func check_left():
  return check_collider(left_shape)
func check_right():
  return check_collider(right_shape)
func check_up():
  return check_collider(up_shape)
func check_down():
  return check_collider(down_shape, 0b11)
