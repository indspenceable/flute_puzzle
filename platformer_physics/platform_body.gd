extends BaseBody
class_name PlatformBody

var snap_amt = 0.0

func grounded():
  # TODO this should be one pixel down...
  return check_collider(down_shape, 0b11 | get_mask(), Vector2.DOWN)

func standing_on_oneway()->bool:
  return check_collider(down_shape, 0b10, Vector2.DOWN) != null

func snap_to_ground() -> float:
  var state := get_world_2d().direct_space_state
  var mask = 0b1000000 | get_mask()
  var rtn = 0
  while !grounded() && check_collider(down_shape, 0b11 | get_mask(), Vector2.DOWN * 5):
    position.y += STEP
    rtn += STEP

  snap_amt = rtn
  emit_signal("changed_position", position.x, position.y)
  emit_signal("changed_position_delta", 0, rtn)
  return rtn
